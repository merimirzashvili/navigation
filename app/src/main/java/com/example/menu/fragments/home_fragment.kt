package com.example.application.fragments



import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation


import com.example.application.R
import java.time.temporal.TemporalAmount

class home_fragment:Fragment (R.layout.fragment_home){

    private lateinit var editTextAmount: EditText
    private lateinit var sendButton:  Button


    override fun onViewCreated(view: View, SavedInstanceState:Bundle?)
    super.onViewCreated(view, savedInstanceState)


    editTextAmount = view.findViewById(R.id.editTextAmount)
    sendButton = view.findViewById(R.id.Buttonsend)

    val controller = Navigation.findNavController(view)


    sendButton.setOnclickedListener {
        val amountText = editTextAmount.text.toString()
        if(amountText.isEmpty()) {
            return@setOnClickListener
            val amount = amountText.toInt()

            val action = home_fragmentDirections.actionHomeFragmentToDeshboardfragment(amount)
            controller.navigate(action)

        }

    }


}