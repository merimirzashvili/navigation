package com.example.application.fragments
import  android.os.Bundle
import android.view.View
import android.widget.TextView

import androidx.fragment.app.Fragment

import com.example.application.R


class deshboard_fragment: Fragment(R.layout.fragment_deshboard){

    override fun onViewCreated(view: View, SavedInstanceState: Bundle?) {


        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.textView).text =
            deshboard_fragmentArgs.fromBundle(requireArguments()).amount.toString()
    }

}